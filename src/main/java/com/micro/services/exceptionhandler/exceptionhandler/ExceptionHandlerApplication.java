package com.micro.services.exceptionhandler.exceptionhandler;

import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptionHandlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExceptionHandlerApplication.class, args);
    }

    static Properties getProperties() {
        var properties = new Properties();
        properties.put("spring.config", "error-properties.yml");
        return properties;
    }
}
