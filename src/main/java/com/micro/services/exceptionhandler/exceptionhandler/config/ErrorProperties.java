package com.micro.services.exceptionhandler.exceptionhandler.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@ConfigurationProperties(prefix = "errormessage")
@PropertySource(value = "classpath:error-properties.yml", factory = YamlPropertiesFactory.class)
public class ErrorProperties {
    private String restClient;
    private String resourceAccess;
    private String unknown;
    private String httpConnection;
    private String server;
    private String unknownHost;
    private String badRequest;
    private String notFound;
    private String unauthorized;
    private String serviceUnavailable;
    private String forbidden;
    private String clientRequest;
}
