package com.micro.services.exceptionhandler.exceptionhandler.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractErrorInfo {

    @JsonProperty("errorInfo")
    private ErrorInfo errorInfo;
    @JsonProperty("timeStamp")
    private Object timeStamp;
}
