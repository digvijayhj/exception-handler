package com.micro.services.exceptionhandler.exceptionhandler.model;

public enum ApiEndPoint {
    MySQl, MongoDb, Rest
}
