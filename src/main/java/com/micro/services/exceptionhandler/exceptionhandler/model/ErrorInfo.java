package com.micro.services.exceptionhandler.exceptionhandler.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorInfo {

    @JsonProperty("errorMessage")
    private String errorMessage;
    @JsonProperty("errorDescription")
    private String errorDescription;
    @JsonProperty("errorCode")
    private String errorCode;
}
