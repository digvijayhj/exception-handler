package com.micro.services.exceptionhandler.exceptionhandler.model;

public class ExceptionLiterals {
    public static final String UNKNOWN_EXCEPTION = "UnknownException";
    public static final String RESOURCE_ACCESS_EXCEPTION = "ResourceAccessException";
    public static final String SERVER_EXCEPTION = "HttpServerException";
    public static final String HTTP_CLIENT_EXCEPTION = "UnknownException";
}
