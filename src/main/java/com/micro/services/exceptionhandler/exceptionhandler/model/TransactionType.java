package com.micro.services.exceptionhandler.exceptionhandler.model;

public enum TransactionType {
    GET, POST, PUT, DELETE, DB
}
