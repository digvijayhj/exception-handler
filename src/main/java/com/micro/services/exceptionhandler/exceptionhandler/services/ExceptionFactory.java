package com.micro.services.exceptionhandler.exceptionhandler.services;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public record ExceptionFactory(
        List<ExceptionResolver> exceptionResolvers) {

    static Map<String, ExceptionResolver> exceptionResolverMap = new HashMap<>();

    @PostConstruct
    public void init() {
        for (ExceptionResolver service :
                exceptionResolvers) {
            exceptionResolverMap.put(service.getType(), service);
        }
    }

    /**
     * Method is used to get exception type from factory of exceptions
     * @param exception
     * @return
     */
    public Optional<ExceptionResolver> getException(String exception) {
        return Optional.ofNullable(exceptionResolverMap.get(exception));
    }
}
