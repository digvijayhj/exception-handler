package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;

public interface ExceptionHandler {

    /**
     * Method utilized to handle specific exception
     *
     * @param exception
     * @param errorMessage
     * @param apiEndPoint
     *
     * @return ErrorInfo
     */
    ErrorInfo getExceptionHandlerResponse(Exception exception, String errorMessage, ApiEndPoint apiEndPoint);
}
