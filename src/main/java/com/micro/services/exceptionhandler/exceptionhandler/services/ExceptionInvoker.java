package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;
import com.micro.services.exceptionhandler.exceptionhandler.model.ExceptionLiterals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("exceptionHandler")
public class ExceptionInvoker implements ExceptionHandler {

    @Autowired
    private ExceptionFactory exceptionFactory;

    @Override
    public ErrorInfo getExceptionHandlerResponse(Exception exception, String errorMessage, ApiEndPoint apiEndPoint) {
        var targetedOperation = exceptionFactory.getException(exception.getClass().getSimpleName())
                .orElse(exceptionFactory.getException(ExceptionLiterals.UNKNOWN_EXCEPTION).get());
        return targetedOperation.getError(exception, errorMessage, apiEndPoint);
    }
}
