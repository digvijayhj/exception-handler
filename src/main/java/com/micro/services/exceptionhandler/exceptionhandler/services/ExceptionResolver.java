package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;

public interface ExceptionResolver {

    String getType();

    ErrorInfo getError(Exception exception, String errorMessage, ApiEndPoint apiEndPoint);
}
