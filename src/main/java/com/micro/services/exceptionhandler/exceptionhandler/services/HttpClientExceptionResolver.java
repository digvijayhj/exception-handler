package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.config.ErrorProperties;
import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;
import com.micro.services.exceptionhandler.exceptionhandler.model.ExceptionLiterals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

public class HttpClientExceptionResolver implements ExceptionResolver{

    @Autowired
    private ErrorProperties errorProperties;

    @Override
    public String getType() {
        return ExceptionLiterals.HTTP_CLIENT_EXCEPTION;
    }

    @Override
    public ErrorInfo getError(Exception exception, String errorMessage, ApiEndPoint apiEndPoint) {
        var excep = (HttpClientErrorException) exception;
        return getErrorInfoResponse(excep, errorMessage, apiEndPoint);
    }

    /**
     * Method is used to set errorInfo specific to error code
     * @param excep
     * @param errorMessage
     * @param apiEndPoint
     * @return ErrorInfo
     */
    private ErrorInfo getErrorInfoResponse(HttpClientErrorException excep, String errorMessage, ApiEndPoint apiEndPoint) {
        var errorInfo = new ErrorInfo();
        switch (excep.getStatusCode()) {
            case BAD_REQUEST -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getBadRequest() + ", " + apiEndPoint + ".");
            case UNAUTHORIZED -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getUnauthorized() + ", " + apiEndPoint + ".");
            case FORBIDDEN -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getForbidden() + ", " + apiEndPoint + ".");
            case NOT_FOUND -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getNotFound() + ", " + apiEndPoint + ".");
            default -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getClientRequest() + ", " + apiEndPoint + ".");
        }
        return errorInfo;
    }
}
