package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.config.ErrorProperties;
import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;
import com.micro.services.exceptionhandler.exceptionhandler.model.ExceptionLiterals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

public class HttpServerExceptionResolver implements ExceptionResolver{

    @Autowired
    private ErrorProperties errorProperties;

    @Override
    public String getType() {
        return ExceptionLiterals.SERVER_EXCEPTION;
    }

    @Override
    public ErrorInfo getError(Exception exception, String errorMessage, ApiEndPoint apiEndPoint) {
        var httpServerErrorException = (HttpServerErrorException) exception;
        var errorInfo = new ErrorInfo();

        switch (httpServerErrorException.getStatusCode()) {
            case INTERNAL_SERVER_ERROR -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getServer() + ", " + apiEndPoint + ".");
            case SERVICE_UNAVAILABLE -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getServiceUnavailable() + ", " + apiEndPoint + ".");
            default -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getServer() + ", " + apiEndPoint + ".");
        }
        return errorInfo;
    }
}
