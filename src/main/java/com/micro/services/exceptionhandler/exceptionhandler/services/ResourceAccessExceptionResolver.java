package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.config.ErrorProperties;
import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;
import com.micro.services.exceptionhandler.exceptionhandler.model.ExceptionLiterals;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class ResourceAccessExceptionResolver implements ExceptionResolver{

    @Autowired
    private ErrorProperties errorProperties;

    @Override
    public String getType() {
        return ExceptionLiterals.RESOURCE_ACCESS_EXCEPTION;
    }

    @Override
    public ErrorInfo getError(Exception exception, String errorMessage, ApiEndPoint apiEndPoint) {
        var errorInfo = new ErrorInfo();
        switch (exception) {
            case SocketTimeoutException se -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getHttpConnection() + ", " + apiEndPoint + ".");
            case ConnectException ce -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getHttpConnection() + ", " + apiEndPoint + ".");
            case UnknownHostException ue -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getUnknownHost() + ", " + apiEndPoint + ".");
            default -> errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getResourceAccess() + ", " + apiEndPoint + ".");
        }
        return errorInfo;
    }
}
