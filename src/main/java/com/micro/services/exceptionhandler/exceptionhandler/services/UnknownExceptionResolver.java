package com.micro.services.exceptionhandler.exceptionhandler.services;

import com.micro.services.exceptionhandler.exceptionhandler.config.ErrorProperties;
import com.micro.services.exceptionhandler.exceptionhandler.model.ApiEndPoint;
import com.micro.services.exceptionhandler.exceptionhandler.model.ErrorInfo;
import com.micro.services.exceptionhandler.exceptionhandler.model.ExceptionLiterals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnknownExceptionResolver implements ExceptionResolver{

    @Autowired
    private ErrorProperties errorProperties;

    @Override
    public String getType() {
        return ExceptionLiterals.UNKNOWN_EXCEPTION;
    }

    @Override
    public ErrorInfo getError(Exception exception, String errorMessage, ApiEndPoint apiEndPoint) {
        var errorInfo = new ErrorInfo();
        errorInfo.setErrorMessage(errorMessage + ", " + errorProperties.getUnknown() + ", " + apiEndPoint + ".");
        return errorInfo;
    }
}
